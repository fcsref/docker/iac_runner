FROM fedora
# COPY docker-ce.repo /etc/yum.repos.d/docker-ce.repo

# Use bash as the default shell
SHELL ["/bin/bash", "-c", "-o", "pipefail"]
ENV VERSION_PACKER="1.7.4"
ENV VERSION_TERRAFORM="1.0.5"

RUN yum -y update && yum -y install git unzip openssh-clients \
    yum-utils device-mapper-persistent-data which \
    python3.8 python3-pip gettext \
    iproute nmap \
    lvm2  &&\
    yum clean all
RUN yum -y install s3cmd && yum clean all
RUN yum -y install jq && yum clean all

RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir setuptools
# hadolint ignore=SC2102
RUN pip3 install --no-cache-dir ansible ansible-lint hvac[parser] netaddr
RUN python3 --version

RUN ansible-inventory --version

RUN pip --version

# install terraform
RUN curl -o /tmp/terraform_${VERSION_TERRAFORM}_linux_amd64.zip https://releases.hashicorp.com/terraform/${VERSION_TERRAFORM}/terraform_${VERSION_TERRAFORM}_linux_amd64.zip
RUN unzip /tmp/terraform_${VERSION_TERRAFORM}_linux_amd64.zip -d /usr/local/bin/
RUN terraform -v
RUN rm -rf /tmp/terraform_${VERSION_TERRAFORM}_linux_amd64.zip

# install packer
RUN curl -o "/tmp/packer_${VERSION_PACKER}_linux_amd64.zip" "https://releases.hashicorp.com/packer/${VERSION_PACKER}/packer_${VERSION_PACKER}_linux_amd64.zip"
RUN ls -alh  "/tmp/packer_${VERSION_PACKER}_linux_amd64.zip"
RUN unzip "/tmp/packer_${VERSION_PACKER}_linux_amd64.zip" -d /usr/local/bin/
RUN packer -v
RUN rm -rf "/tmp/packer_${VERSION_PACKER}_linux_amd64.zip"

# install go
RUN curl -o /tmp/go1.13.linux-amd64.tar.gz https://dl.google.com/go/go1.13.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf /tmp/go1.13.linux-amd64.tar.gz
RUN ln -s /usr/local/go/bin/go /usr/local/bin
RUN /usr/local/go/bin/go version
RUN rm -rf /tmp/go1.13.linux-amd64.tar.gz

# install tfdocs
RUN GO111MODULE="on" go get github.com/terraform-docs/terraform-docs@v0.10.1
RUN mv /root/go/bin/terraform-docs /usr/local/bin/
RUN terraform-docs -v

# install tflint
RUN curl -o /tmp/tflint.zip -L https://github.com/terraform-linters/tflint/releases/download/v0.20.2/tflint_linux_amd64.zip
RUN unzip /tmp/tflint.zip -d /usr/local/bin/
RUN tflint -v
RUN rm /tmp/tflint.zip

# install tfsec
# RUN GO111MODULE=on go get -u github.com/tfsec/tfsec/cmd/tfsec
RUN curl -LJO https://github.com/tfsec/tfsec/releases/download/v0.40.5/tfsec-linux-amd64
RUN mv ./tfsec-linux-amd64 /usr/local/bin/tfsec && chmod +x /usr/local/bin/tfsec
# RUN mv /root/go/bin/tfsec /usr/local/bin/
RUN tfsec -v

# install vault
RUN curl -o /tmp/vault_1.6.0-rc_linux_amd64.zip https://releases.hashicorp.com/vault/1.6.0-rc/vault_1.6.0-rc_linux_amd64.zip
RUN unzip /tmp/vault_1.6.0-rc_linux_amd64.zip -d /usr/local/bin/
RUN vault -v
RUN rm -rf /tmp/vault_1.6.0-rc_linux_amd64.zip

# install aws cli
# RUN curl -o /tmp/awscli-bundle.zip https://s3.amazonaws.com/aws-cli/awscli-bundle.zip
# RUN mkdir /tmp/aws
# RUN unzip tmp/awscli-bundle.zip -d /tmp/aws
# RUN /tmp/aws/awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
# RUN rm -rfv /tmp/aws /tmp/awscli-bundle.zip
RUN pip3 install awscli

# install kubectl
RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl
RUN kubectl version --client

# install helm
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
RUN chmod 700 get_helm.sh
RUN VERIFY_CHECKSUM="false" ./get_helm.sh

# clean up
RUN rm -rf /root/go


RUN git config --global user.email "terraform@bot.null" && \
    git config --global user.name "Terraform"

RUN ansible-galaxy install zauberpony.mysql-query
RUN ansible-galaxy collection install ansible.posix
RUN ansible-galaxy collection install community.mysql
RUN ansible-galaxy collection install ansible.posix

EXPOSE 8000-8010

